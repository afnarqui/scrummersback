require('dotenv').config();
const sql = require('mssql');
const config = require('../../config');

const query = {};

/**
 * Gets the availability for a room type for a set of days
 */
query.get = (request, res, params) => {
  const conn = new sql.ConnectionPool(config);
  let nameProcedure;
  conn.connect(function (err) {
    if (err) throw err;
    const req = new sql.Request(conn);
    if (params.id !== undefined) {
      let Id = params.id;
      let dateInitial = params.dateInitial;
      let dateEnd = params.dateEnd;
      req.input('id', sql.Int, Id);
      req.input('dateInitial', sql.VarChar(30), dateInitial);
      req.input('dateEnd', sql.VarChar(30), dateEnd);
      nameProcedure = 'searchOneAvailability';
    } else {
      nameProcedure = 'searchAvailability';
    }
    req.execute(nameProcedure, function (err, response) {
      if (err) throw err;
      else conn.close();
      return res.status(200).send(response.recordset);
    });
  });
};

/**
 * Makes a reservation for a room
 */
query.post = (request, res, params) => {
  let { id, dateInitial, dateEnd } = params;

  let nameProcedure = 'addReservation';

  let conn = new sql.ConnectionPool(config);
  conn.connect(function (err) {
    if (err) throw err;
    let req = new sql.Request(conn);
    req.input('id', sql.Int, id);
    req.input('dateInitial', sql.VarChar(30), dateInitial);
    req.input('dateEnd', sql.VarChar(30), dateEnd);
    req.execute(nameProcedure, function (err, response) {
      if (err) throw err;
      else conn.close();
      return res.send(response.recordset);
    });
  });
};

/**
 * Updates an existing reservation
 */
query.put = (request, res, params) => {
  let { id, reservationId, dateInitial, dateEnd } = params;

  let nameProcedure = 'updateReservation';

  let conn = new sql.ConnectionPool(config);
  conn.connect(function (err) {
    if (err) throw err;
    let req = new sql.Request(conn);
    req.input('id', sql.Int, id);
    req.input('reservationId', sql.Int, reservationId);
    req.input('dateInitial', sql.VarChar(30), dateInitial);
    req.input('dateEnd', sql.VarChar(30), dateEnd);
    req.execute(nameProcedure, function (err, response) {
      if (err) throw err;
      else conn.close();
      return res.send(response.recordset);
    });
  });
};

/**
 * Deletes an existing reservation
 */
query.delete = (request, res, params) => {
  let { id } = params;

  let nameProcedure = 'deleteReservation';

  let conn = new sql.ConnectionPool(config);
  conn.connect(function (err) {
    if (err) throw err;
    let req = new sql.Request(conn);
    req.input('id', sql.Int, id);
    req.execute(nameProcedure, function (err, response) {
      if (err) throw err;
      else conn.close();
      return res.send(response.recordset);
    });
  });
};

/**
 * Gets all rooms
 */
query.getRooms = (request, res, params) => {
  const conn = new sql.ConnectionPool(config);
  let nameProcedure = 'searchRooms';
  conn.connect(function (err) {
    if (err) throw err;
    const req = new sql.Request(conn);
    req.execute(nameProcedure, function (err, response) {
      if (err) throw err;
      else conn.close();
      return res.status(200).send(response.recordset);
    });
  });
};

module.exports = query;
