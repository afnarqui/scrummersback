create procedure searchAvailability
as

select
 res.reservationId as id,
 res.availabilityId,
a.roomsId as rooms,
res.checkin,
res.checkout,
r.name,
r.beds
from availability a
inner join rooms r on r.roomsId = a.roomsId
inner join reservation res on res.availabilityId = a.availabilityId