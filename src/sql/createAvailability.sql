
CREATE TABLE [dbo].[availability](
	[availabilityId] [bigint] IDENTITY(1,1) NOT NULL,
	[roomsId] [bigint] NOT NULL,
	[checkin] [datetime] NOT NULL,
	[checkout] [datetime] NOT NULL,
	[state] [bit] NOT NULL CONSTRAINT [DF_availability_state]  DEFAULT ((0)),
 CONSTRAINT [PK_availability] PRIMARY KEY CLUSTERED 
(
	[availabilityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[availability]  WITH CHECK ADD  CONSTRAINT [FK_availability_rooms] FOREIGN KEY([roomsId])
REFERENCES [dbo].[rooms] ([roomsId])
GO

ALTER TABLE [dbo].[availability] CHECK CONSTRAINT [FK_availability_rooms]
GO

