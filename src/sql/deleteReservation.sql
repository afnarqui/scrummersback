create procedure deleteReservation
@id int
as

	declare @availabilityId  int
	select @availabilityId = availabilityId from  reservation
		where reservationId =@id

	if exists(select top 1 1 from reservation r inner join availability a
				on r.availabilityId = a.availabilityId
				where r.reservationId = @id and a.availabilityId = @availabilityId
				)
	begin
		delete reservation 
			where reservationId =@id

		delete availability 
		where availabilityId = @availabilityId
	
	
		
		select 'values deleted with success' as data
	end
	else
	begin
		select 'please change the values of rooms' as data
	end