 create procedure updateReservation
@id int, @reservationId int, @dateInitial datetime, @dateEnd datetime
as


if convert(char,@dateInitial,112) > convert(char,@dateEnd,112)
begin
	select 'The end date must be higher than the start date' as data
return 
end

if exists(
select 
	top 1 1 from availability a
  where  a.state = 1
  and a.roomsId  = @id
  	and convert(char,a.checkin,112) >= convert(char,@dateInitial,112)
	and convert(char,a.checkin,112) <= convert(char,@dateEnd,112))
	begin

	select 'please change the values of date' as data
end
 else
begin
	declare @availabilityId  int
	select @availabilityId = availabilityId from  reservation
		where reservationId =@reservationId

	if exists(select top 1 1 from reservation r inner join availability a
				on r.availabilityId = a.availabilityId
				where r.reservationId = @reservationId and a.roomsId = @id
				)
	begin
		update availability set checkin =@dateInitial, checkout=@dateEnd
		where availabilityId = @availabilityId
	
		update reservation set checkin=@dateInitial ,checkout=@dateEnd
			where reservationId =@reservationId
		
		select 'values update with success' as data
	end
	else
	begin
		select 'please change the values of rooms' as data
	end

end