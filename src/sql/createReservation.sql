CREATE TABLE [dbo].[reservation](
	[reservationId] [bigint] IDENTITY(1,1) NOT NULL,
	[availabilityId] [bigint] NOT NULL,
	[checkin] [datetime] NOT NULL,
	[checkout] [datetime] NULL,
 CONSTRAINT [PK_reservation] PRIMARY KEY CLUSTERED 
(
	[reservationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[reservation]  WITH CHECK ADD  CONSTRAINT [FK_reservation_availability] FOREIGN KEY([availabilityId])
REFERENCES [dbo].[availability] ([availabilityId])
GO

ALTER TABLE [dbo].[reservation] CHECK CONSTRAINT [FK_reservation_availability]
GO

