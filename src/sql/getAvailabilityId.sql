create procedure searchOneAvailability
@id int, @dateInitial datetime, @dateEnd datetime
as

if convert(char,@dateInitial,112) > convert(char,@dateEnd,112)
begin
	select @id as roomsId, 0 as state,'The end date must be higher than the start date' as name
return 
end


if exists(
select 
	r.roomsId,
	'Availability' as state
	
from rooms r
where r.roomsId = @id
and r.roomsId not in
 (select a.roomsId from availability a
  where  a.state = 1
and (   convert(char,a.checkin,112) >= convert(char,@dateInitial,112)
	and convert(char,a.checkin,112) <= convert(char,@dateEnd,112))
)
)
begin
 select  @id as roomsId, 1 as state, 'availability' as name
end
else
begin
 select  @id as roomsId, 0 as state, 'not availability' as name
end
