create procedure addReservation
@id int, @dateInitial datetime, @dateEnd datetime
as


if convert(char,@dateInitial,112) > convert(char,@dateEnd,112)
begin
	select 'The end date must be higher than the start date' as data
return 
end


if exists(
select 
	top 1 1 from availability a
  where  a.state = 1
  and a.roomsId  = @id
  	and convert(char,a.checkin,112) >= convert(char,@dateInitial,112)
	and convert(char,a.checkin,112) <= convert(char,@dateEnd,112))
	begin

	select 'please change the values of date' as data
end
 else
begin
	declare @idNuevo int

	insert into availability(roomsId,checkin,checkout,state)
	values(@id,@dateInitial,@dateEnd,1)

	set @idNuevo = scope_identity()
	
	insert into reservation(availabilityId,checkin,checkout)
	values (@idNuevo,@dateInitial,@dateEnd)

	select 'values inserted with success' as data
end