const availability = require('../models');

const query = {};

/**
 * Gets the availability for a room type for a set of days
 */
query.get = (req, res) => {
  availability.get(req, res, req.query);
};

/**
 * Makes a reservation for a room
 */
query.post = (req, res) => {
  availability.post(req, res, req.query);
};

/**
 * Updates an existing reservation
 */
query.put = (req, res) => {
  availability.put(req, res, req.query);
};

/**
 * Deletes an existing reservation
 */
query.delete = (req, res) => {
  availability.delete(req, res, req.query);
};

/**
 * Gets all rooms
 */
query.getRooms = (req, res) => {
  availability.getRooms(req, res, req.query);
};

module.exports = query;
