const express = require('express');
const availability = require('../controllers/availability');

const router = express.Router();

/**
 * Gets the availability for a room type for a set of days
 */
router.get('/api/room/availability', function (req, res) {
  availability.get(req, res, req.body);
});

/**
 * Makes a reservation for a room
 */
router.post('/api/room/reservation', function (req, res) {
  availability.post(req, res, req.body);
});

/**
 * Updates an existing reservation
 */
router.put('/api/room/reservation', function (req, res) {
  availability.put(req, res, req.body);
});

/**
 * Deletes an existing reservation
 */
router.delete('/api/room/reservation', function (req, res) {
  availability.delete(req, res, req.body);
});

/**
 * Gets all rooms
 */
router.get('/api/room/rooms', function (req, res) {
  availability.getRooms(req, res, req.body);
});

module.exports = router;
