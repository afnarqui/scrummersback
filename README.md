# scrummersback

[![N|Solid](https://firebasestorage.googleapis.com/v0/b/andresnaranjo-marcapersonal.appspot.com/o/prueba-sabado%2Fget.JPG?alt=media&token=83d73fa9-ed9a-4b9a-b317-ad55e2eaae0c)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

project scrummersback. developed in nodejs with sql server using express

[trello](https://trello.com/b/GbenLBcC/scrummers) Trello

And of course scrummersback itself is open source with a [public repository][afn]
on GitLab.

## Usage

scrummersback requires

[Git](https://git-scm.com/downloads)

to run.

Install the dependencies and devDependencies previous
start the server.

## download

```sh
git clone https://gitlab.com/afnarqui/scrummersback
cd scrummersback
npm start in production
npm dev in developement
```

## run aplication in developement

```sh
http://localhost:8400/
```

## run aplication in production

```sh
http://138.121.170.105:8400/
```

[afn]: https://gitlab.com/afnarqui/scrummersback
